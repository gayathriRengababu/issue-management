'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketHistorySchema = new Schema({
  ticket_id : {type :mongoose.type.ObjectId , ref : 'Ticket'},
    prd_chg :{type : Boolean},
    prd_chg_from : {type : String},
    prd_chg_to : {type : String},
    req_cat_chg :{type : Boolean},
    req_cat_from : {type : String},
    req_cat_to : {type : String},
    priority_chg :{type : Boolean},
    priority_from : {type : String},
    priority_to : {type : String},
    status_chg :{type : Boolean},
    status_from : {type : String},
    status_to : {type : String},
    assign_grp_chg :{type : Boolean},
    assign_grp_from : {type : String},
    assign_grp_to : {type : String},
    assign_wr_chg :{type : Boolean},
    assign_wr_from : {type : String},
    assign_wr_to : {type : String},
    
});

module.exports = mongoose.model('TicketHistory', TicketHistorySchema);
