'use strict';

var _ = require('lodash');
var TicketHistory = require('./ticket-history.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /ticketHistorys Get a list of ticketHistorys
 * @apiVersion 0.1.0
 * @apiName GetTicketHistorys
 * @apiDescription Get all the ticketHistory.
 * @apiGroup TicketHistorys
 *
 */
exports.index = function (req, res) {
  TicketHistory.find(function (err, ticketHistorys) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(ticketHistorys);
  });
};

/**
 * @api {get} /ticketHistorys/:id Get a single ticketHistory
 * @apiVersion 0.1.0
 * @apiName GetTicketHistory
 * @apiDescription Get only one unique element of ticketHistory based on its unique id.
 * @apiGroup TicketHistorys
 *
 * @apiParam {String} id TicketHistorys unique id.
 *
 */
exports.show = function (req, res) {
  TicketHistory.findById(req.params.id, function (err, ticketHistory) {
    if (err) { return handleError(res, err); }
    if (!ticketHistory) { return res.status(404).end(); }
    return res.status(200).json(ticketHistory);
  });
};

/**
 * @api {post} /ticketHistorys Create a new ticketHistory
 * @apiVersion 0.1.0
 * @apiName CreateTicketHistory
 * @apiDescription Creates a new ticketHistory.
 * @apiGroup TicketHistorys
 *
 */
exports.create = function (req, res) {
  TicketHistory.create(req.body, function (err, ticketHistory) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(ticketHistory);
  });
};

/**
 * @api {post} /ticketHistory/search Search TicketHistory
 * @apiVersion 0.1.0
 * @apiName searchTicketHistory
 * @apiDescription Search for any element in TicketHistory
 * @apiGroup TicketHistory
 *
 * @apiParam {String} SearchCriteria
 *
 */
exports.search = function (req, res) {
  
    TicketHistory.find(req.body.SearchCriteria,function(err,ticketHistory){
         if (err) res.json({ status: 'error', message:err });
        if(ticketHistory){
            return res.status(200).json(ticketHistory);

        }
        
    });
};
/**
 * @api {put} /ticketHistorys/:id Updates an existing ticketHistory
 * @apiVersion 0.1.0
 * @apiName UpdateTicketHistory
 * @apiDescription Update an element of ticketHistory based on its unique id.
 * @apiGroup TicketHistorys
 *
 * @apiParam {String} id TicketHistorys unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  TicketHistory.findById(req.params.id, function (err, ticketHistory) {
    if (err) { return handleError(res, err); }
    if (!ticketHistory) { return res.status(404).end(); }
    var updated = _.merge(ticketHistory, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ticketHistory);
    });
  });
};

/**
 * @api {delete} /ticketHistorys/:id Deletes a ticketHistory
 * @apiVersion 0.1.0
 * @apiName RemoveTicketHistory
 * @apiDescription Delete an element of ticketHistory based on its unique id.
 * @apiGroup TicketHistorys
 *
 * @apiParam {String} id TicketHistorys unique id.
 *
 */
exports.destroy = function (req, res) {
  TicketHistory.findById(req.params.id, function (err, ticketHistory) {
    if (err) { return handleError(res, err); }
    if (!ticketHistory) { return res.status(404).end(); }
    ticketHistory.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
