'use strict';

var Product = require('./product.model');

exports.register = function (socket) {

  Product.schema.post('save', function (doc) {
    socket.emit('Product:save', doc);
  });

  Product.schema.post('remove', function (doc) {
    socket.emit('Product:remove', doc);
  });

};
