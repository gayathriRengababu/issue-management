'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
    product_name: {type : String ,required : true},
    product_url : {type : String}
});

module.exports = mongoose.model('Product', ProductSchema);
