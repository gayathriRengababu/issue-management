'use strict';

var _ = require('lodash');
var Product = require('./product.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /products Get a list of products
 * @apiVersion 0.1.0
 * @apiName GetProducts
 * @apiDescription Get all the product.
 * @apiGroup Products
 *
 */
exports.index = function (req, res) {
  Product.find(function (err, products) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(products);
  });
};

/**
 * @api {get} /products/:id Get a single product
 * @apiVersion 0.1.0
 * @apiName GetProduct
 * @apiDescription Get only one unique element of product based on its unique id.
 * @apiGroup Products
 *
 * @apiParam {String} id Products unique id.
 *
 */
exports.show = function (req, res) {
  Product.findById(req.params.id, function (err, product) {
    if (err) { return handleError(res, err); }
    if (!product) { return res.status(404).end(); }
    return res.status(200).json(product);
  });
};

/**
 * @api {post} /products Create a new product
 * @apiVersion 0.1.0
 * @apiName CreateProduct
 * @apiDescription Creates a new product.
 * @apiGroup Products
 *
 */
exports.create = function (req, res) {
  Product.create(req.body, function (err, product) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(product);
  });
};

/**
 * @api {post} /product/search Search Product
 * @apiVersion 0.1.0
 * @apiName searchProduct
 * @apiDescription Search for any element in Product
 * @apiGroup Product
 *
 * @apiParam {String} SearchCriteria
 *
 */
exports.search = function (req, res) {
  
    Product.find(req.body.SearchCriteria,function(err,product){
         if (err) res.json({ status: 'error', message:err });
        if(product){
            return res.status(200).json(product);

        }
        
    });
};

/**
 * @api {put} /products/:id Updates an existing product
 * @apiVersion 0.1.0
 * @apiName UpdateProduct
 * @apiDescription Update an element of product based on its unique id.
 * @apiGroup Products
 *
 * @apiParam {String} id Products unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Product.findById(req.params.id, function (err, product) {
    if (err) { return handleError(res, err); }
    if (!product) { return res.status(404).end(); }
    var updated = _.merge(product, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(product);
    });
  });
};

/**
 * @api {delete} /products/:id Deletes a product
 * @apiVersion 0.1.0
 * @apiName RemoveProduct
 * @apiDescription Delete an element of product based on its unique id.
 * @apiGroup Products
 *
 * @apiParam {String} id Products unique id.
 *
 */
exports.destroy = function (req, res) {
  Product.findById(req.params.id, function (err, product) {
    if (err) { return handleError(res, err); }
    if (!product) { return res.status(404).end(); }
    product.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
