'use strict';

var _ = require('lodash');
var TicketAttachment = require('./ticket-attachment.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /ticketAttachments Get a list of ticketAttachments
 * @apiVersion 0.1.0
 * @apiName GetTicketAttachments
 * @apiDescription Get all the ticketAttachment.
 * @apiGroup TicketAttachments
 *
 */
exports.index = function (req, res) {
  TicketAttachment.find(function (err, ticketAttachments) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(ticketAttachments);
  });
};

/**
 * @api {get} /ticketAttachments/:id Get a single ticketAttachment
 * @apiVersion 0.1.0
 * @apiName GetTicketAttachment
 * @apiDescription Get only one unique element of ticketAttachment based on its unique id.
 * @apiGroup TicketAttachments
 *
 * @apiParam {String} id TicketAttachments unique id.
 *
 */
exports.show = function (req, res) {
  TicketAttachment.findById(req.params.id, function (err, ticketAttachment) {
    if (err) { return handleError(res, err); }
    if (!ticketAttachment) { return res.status(404).end(); }
    return res.status(200).json(ticketAttachment);
  });
};

/**
 * @api {post} /ticketAttachments Create a new ticketAttachment
 * @apiVersion 0.1.0
 * @apiName CreateTicketAttachment
 * @apiDescription Creates a new ticketAttachment.
 * @apiGroup TicketAttachments
 *
 */
exports.create = function (req, res) {
  TicketAttachment.create(req.body, function (err, ticketAttachment) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(ticketAttachment);
  });
};

/**
 * @api {put} /ticketAttachments/:id Updates an existing ticketAttachment
 * @apiVersion 0.1.0
 * @apiName UpdateTicketAttachment
 * @apiDescription Update an element of ticketAttachment based on its unique id.
 * @apiGroup TicketAttachments
 *
 * @apiParam {String} id TicketAttachments unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  TicketAttachment.findById(req.params.id, function (err, ticketAttachment) {
    if (err) { return handleError(res, err); }
    if (!ticketAttachment) { return res.status(404).end(); }
    var updated = _.merge(ticketAttachment, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ticketAttachment);
    });
  });
};

/**
 * @api {delete} /ticketAttachments/:id Deletes a ticketAttachment
 * @apiVersion 0.1.0
 * @apiName RemoveTicketAttachment
 * @apiDescription Delete an element of ticketAttachment based on its unique id.
 * @apiGroup TicketAttachments
 *
 * @apiParam {String} id TicketAttachments unique id.
 *
 */
exports.destroy = function (req, res) {
  TicketAttachment.findById(req.params.id, function (err, ticketAttachment) {
    if (err) { return handleError(res, err); }
    if (!ticketAttachment) { return res.status(404).end(); }
    ticketAttachment.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
