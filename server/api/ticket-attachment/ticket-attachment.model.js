'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketAttachmentSchema = new Schema({
  title: String,
    path : {type : String},
    created_by : {type : String},
    created_date : {type : Date},
    updated_by : {type : String},
    updated_date : {type : Date},
});

module.exports = mongoose.model('TicketAttachment', TicketAttachmentSchema);
