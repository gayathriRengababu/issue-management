'use strict';

var _ = require('lodash');
var TicketDescription = require('./ticket-description.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /ticketDescriptions Get a list of ticketDescriptions
 * @apiVersion 0.1.0
 * @apiName GetTicketDescriptions
 * @apiDescription Get all the ticketDescription.
 * @apiGroup TicketDescriptions
 *
 */
exports.index = function (req, res) {
  TicketDescription.find(function (err, ticketDescriptions) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(ticketDescriptions);
  });
};

/**
 * @api {get} /ticketDescriptions/:id Get a single ticketDescription
 * @apiVersion 0.1.0
 * @apiName GetTicketDescription
 * @apiDescription Get only one unique element of ticketDescription based on its unique id.
 * @apiGroup TicketDescriptions
 *
 * @apiParam {String} id TicketDescriptions unique id.
 *
 */
exports.show = function (req, res) {
  TicketDescription.findById(req.params.id, function (err, ticketDescription) {
    if (err) { return handleError(res, err); }
    if (!ticketDescription) { return res.status(404).end(); }
    return res.status(200).json(ticketDescription);
  });
};

/**
 * @api {post} /ticketDescriptions Create a new ticketDescription
 * @apiVersion 0.1.0
 * @apiName CreateTicketDescription
 * @apiDescription Creates a new ticketDescription.
 * @apiGroup TicketDescriptions
 *
 */
exports.create = function (req, res) {
  TicketDescription.create(req.body, function (err, ticketDescription) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(ticketDescription);
  });
};

/**
 * @api {put} /ticketDescriptions/:id Updates an existing ticketDescription
 * @apiVersion 0.1.0
 * @apiName UpdateTicketDescription
 * @apiDescription Update an element of ticketDescription based on its unique id.
 * @apiGroup TicketDescriptions
 *
 * @apiParam {String} id TicketDescriptions unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  TicketDescription.findById(req.params.id, function (err, ticketDescription) {
    if (err) { return handleError(res, err); }
    if (!ticketDescription) { return res.status(404).end(); }
    var updated = _.merge(ticketDescription, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ticketDescription);
    });
  });
};

/**
 * @api {delete} /ticketDescriptions/:id Deletes a ticketDescription
 * @apiVersion 0.1.0
 * @apiName RemoveTicketDescription
 * @apiDescription Delete an element of ticketDescription based on its unique id.
 * @apiGroup TicketDescriptions
 *
 * @apiParam {String} id TicketDescriptions unique id.
 *
 */
exports.destroy = function (req, res) {
  TicketDescription.findById(req.params.id, function (err, ticketDescription) {
    if (err) { return handleError(res, err); }
    if (!ticketDescription) { return res.status(404).end(); }
    ticketDescription.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
