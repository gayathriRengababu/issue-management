'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketDescriptionSchema = new Schema({
  ticket_desc: {type : String}
});

module.exports = mongoose.model('TicketDescription', TicketDescriptionSchema);
