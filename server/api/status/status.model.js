'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StatusSchema = new Schema({
    status_desc: String,
    created_by : {type : String},
    created_date : {type : Date}
});

module.exports = mongoose.model('Status', StatusSchema);
