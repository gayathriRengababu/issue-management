'use strict';

var _ = require('lodash');
var Status = require('./status.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /statuss Get a list of statuss
 * @apiVersion 0.1.0
 * @apiName GetStatuss
 * @apiDescription Get all the status.
 * @apiGroup Statuss
 *
 */
exports.index = function (req, res) {
  Status.find(function (err, statuss) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(statuss);
  });
};

/**
 * @api {get} /statuss/:id Get a single status
 * @apiVersion 0.1.0
 * @apiName GetStatus
 * @apiDescription Get only one unique element of status based on its unique id.
 * @apiGroup Statuss
 *
 * @apiParam {String} id Statuss unique id.
 *
 */
exports.show = function (req, res) {
  Status.findById(req.params.id, function (err, status) {
    if (err) { return handleError(res, err); }
    if (!status) { return res.status(404).end(); }
    return res.status(200).json(status);
  });
};

/**
 * @api {post} /statuss Create a new status
 * @apiVersion 0.1.0
 * @apiName CreateStatus
 * @apiDescription Creates a new status.
 * @apiGroup Statuss
 *
 */
exports.create = function (req, res) {
  Status.create(req.body, function (err, status) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(status);
  });
};

/**
 * @api {put} /statuss/:id Updates an existing status
 * @apiVersion 0.1.0
 * @apiName UpdateStatus
 * @apiDescription Update an element of status based on its unique id.
 * @apiGroup Statuss
 *
 * @apiParam {String} id Statuss unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Status.findById(req.params.id, function (err, status) {
    if (err) { return handleError(res, err); }
    if (!status) { return res.status(404).end(); }
    var updated = _.merge(status, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(status);
    });
  });
};

/**
 * @api {delete} /statuss/:id Deletes a status
 * @apiVersion 0.1.0
 * @apiName RemoveStatus
 * @apiDescription Delete an element of status based on its unique id.
 * @apiGroup Statuss
 *
 * @apiParam {String} id Statuss unique id.
 *
 */
exports.destroy = function (req, res) {
  Status.findById(req.params.id, function (err, status) {
    if (err) { return handleError(res, err); }
    if (!status) { return res.status(404).end(); }
    status.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
