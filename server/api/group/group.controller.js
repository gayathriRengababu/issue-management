'use strict';

var _ = require('lodash');
var Group = require('./group.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /groups Get a list of groups
 * @apiVersion 0.1.0
 * @apiName GetGroups
 * @apiDescription Get all the group.
 * @apiGroup Groups
 *
 */
exports.index = function (req, res) {
  Group.find(function (err, groups) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(groups);
  });
};

/**
 * @api {get} /groups/:id Get a single group
 * @apiVersion 0.1.0
 * @apiName GetGroup
 * @apiDescription Get only one unique element of group based on its unique id.
 * @apiGroup Groups
 *
 * @apiParam {String} id Groups unique id.
 *
 */
exports.show = function (req, res) {
  Group.findById(req.params.id, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.status(404).end(); }
    return res.status(200).json(group);
  });
};

/**
 * @api {post} /groups Create a new group
 * @apiVersion 0.1.0
 * @apiName CreateGroup
 * @apiDescription Creates a new group.
 * @apiGroup Groups
 *
 */
exports.create = function (req, res) {
  Group.create(req.body, function (err, group) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(group);
  });
};


/**
 * @api {post} /group/search Search Group
 * @apiVersion 0.1.0
 * @apiName searchGroup
 * @apiDescription Search for any element in Group
 * @apiGroup Group
 *
 * @apiParam {String} SearchCriteria
 *
 */
exports.search = function (req, res) {
  
    Group.find(req.body.SearchCriteria,function(err,group){
         if (err) res.json({ status: 'error', message:err });
        if(group){
            return res.status(200).json(group);

        }
        
    });
};
/**
 * @api {put} /groups/:id Updates an existing group
 * @apiVersion 0.1.0
 * @apiName UpdateGroup
 * @apiDescription Update an element of group based on its unique id.
 * @apiGroup Groups
 *
 * @apiParam {String} id Groups unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Group.findById(req.params.id, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.status(404).end(); }
    var updated = _.merge(group, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(group);
    });
  });
};

/**
 * @api {delete} /groups/:id Deletes a group
 * @apiVersion 0.1.0
 * @apiName RemoveGroup
 * @apiDescription Delete an element of group based on its unique id.
 * @apiGroup Groups
 *
 * @apiParam {String} id Groups unique id.
 *
 */
exports.destroy = function (req, res) {
  Group.findById(req.params.id, function (err, group) {
    if (err) { return handleError(res, err); }
    if (!group) { return res.status(404).end(); }
    group.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
