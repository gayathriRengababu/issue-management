'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GroupSchema = new Schema({
  client_id : {type: mongoose.type.objectId , ref: 'Client'},
    group_name : {type : String},
    created_by : {type : String},
    created_date : {type : Date},
    owner : {type : String}
    
});

module.exports = mongoose.model('Group', GroupSchema);
