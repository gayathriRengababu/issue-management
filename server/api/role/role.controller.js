'use strict';

var _ = require('lodash');
var Role = require('./role.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /roles Get a list of roles
 * @apiVersion 0.1.0
 * @apiName GetRoles
 * @apiDescription Get all the role.
 * @apiGroup Roles
 *
 */
exports.index = function (req, res) {
  Role.find(function (err, roles) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(roles);
  });
};

/**
 * @api {get} /roles/:id Get a single role
 * @apiVersion 0.1.0
 * @apiName GetRole
 * @apiDescription Get only one unique element of role based on its unique id.
 * @apiGroup Roles
 *
 * @apiParam {String} id Roles unique id.
 *
 */
exports.show = function (req, res) {
  Role.findById(req.params.id, function (err, role) {
    if (err) { return handleError(res, err); }
    if (!role) { return res.status(404).end(); }
    return res.status(200).json(role);
  });
};

/**
 * @api {post} /roles Create a new role
 * @apiVersion 0.1.0
 * @apiName CreateRole
 * @apiDescription Creates a new role.
 * @apiGroup Roles
 *
 */
exports.create = function (req, res) {
  Role.create(req.body, function (err, role) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(role);
  });
};

/**
 * @api {put} /roles/:id Updates an existing role
 * @apiVersion 0.1.0
 * @apiName UpdateRole
 * @apiDescription Update an element of role based on its unique id.
 * @apiGroup Roles
 *
 * @apiParam {String} id Roles unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Role.findById(req.params.id, function (err, role) {
    if (err) { return handleError(res, err); }
    if (!role) { return res.status(404).end(); }
    var updated = _.merge(role, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(role);
    });
  });
};

/**
 * @api {delete} /roles/:id Deletes a role
 * @apiVersion 0.1.0
 * @apiName RemoveRole
 * @apiDescription Delete an element of role based on its unique id.
 * @apiGroup Roles
 *
 * @apiParam {String} id Roles unique id.
 *
 */
exports.destroy = function (req, res) {
  Role.findById(req.params.id, function (err, role) {
    if (err) { return handleError(res, err); }
    if (!role) { return res.status(404).end(); }
    role.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
