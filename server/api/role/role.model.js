'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema({
    role_desc: {type: String},
    access_url : {type: String}
});

module.exports = mongoose.model('Role', RoleSchema);
