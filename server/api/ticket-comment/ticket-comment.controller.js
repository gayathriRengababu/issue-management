'use strict';

var _ = require('lodash');
var TicketComment = require('./ticket-comment.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /ticketComments Get a list of ticketComments
 * @apiVersion 0.1.0
 * @apiName GetTicketComments
 * @apiDescription Get all the ticketComment.
 * @apiGroup TicketComments
 *
 */
exports.index = function (req, res) {
  TicketComment.find(function (err, ticketComments) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(ticketComments);
  });
};

/**
 * @api {get} /ticketComments/:id Get a single ticketComment
 * @apiVersion 0.1.0
 * @apiName GetTicketComment
 * @apiDescription Get only one unique element of ticketComment based on its unique id.
 * @apiGroup TicketComments
 *
 * @apiParam {String} id TicketComments unique id.
 *
 */
exports.show = function (req, res) {
  TicketComment.findById(req.params.id, function (err, ticketComment) {
    if (err) { return handleError(res, err); }
    if (!ticketComment) { return res.status(404).end(); }
    return res.status(200).json(ticketComment);
  });
};

/**
 * @api {post} /ticketComments Create a new ticketComment
 * @apiVersion 0.1.0
 * @apiName CreateTicketComment
 * @apiDescription Creates a new ticketComment.
 * @apiGroup TicketComments
 *
 */
exports.create = function (req, res) {
  TicketComment.create(req.body, function (err, ticketComment) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(ticketComment);
  });
};

/**
 * @api {put} /ticketComments/:id Updates an existing ticketComment
 * @apiVersion 0.1.0
 * @apiName UpdateTicketComment
 * @apiDescription Update an element of ticketComment based on its unique id.
 * @apiGroup TicketComments
 *
 * @apiParam {String} id TicketComments unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  TicketComment.findById(req.params.id, function (err, ticketComment) {
    if (err) { return handleError(res, err); }
    if (!ticketComment) { return res.status(404).end(); }
    var updated = _.merge(ticketComment, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ticketComment);
    });
  });
};

/**
 * @api {delete} /ticketComments/:id Deletes a ticketComment
 * @apiVersion 0.1.0
 * @apiName RemoveTicketComment
 * @apiDescription Delete an element of ticketComment based on its unique id.
 * @apiGroup TicketComments
 *
 * @apiParam {String} id TicketComments unique id.
 *
 */
exports.destroy = function (req, res) {
  TicketComment.findById(req.params.id, function (err, ticketComment) {
    if (err) { return handleError(res, err); }
    if (!ticketComment) { return res.status(404).end(); }
    ticketComment.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
