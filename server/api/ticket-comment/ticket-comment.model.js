'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketCommentSchema = new Schema({
  ticket_id : {type :mongoose.type.ObjectId , ref : 'Ticket'},
    created_by : {type : String},
    created_date : {type : Date},
    updated_by : {type : String},
    updated_date : {type : Date},
    attachment_id : {type :mongoose.type.ObjectId , ref : 'TicketAttachment'},
    comment : {type : String}
});

module.exports = mongoose.model('TicketComment', TicketCommentSchema);
