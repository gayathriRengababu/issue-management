'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReqCategorySchema = new Schema({
  client_id : {type: mongoose.type.objectId , ref: 'Client'},
    req_cat_desc : {type : String},
    created_by : {type : String},
    created_date : {type : Date},
    updated_by : {type : String},
    updated_date : {type : Date},
});

module.exports = mongoose.model('ReqCategory', ReqCategorySchema);
