'use strict';

var _ = require('lodash');
var ReqCategory = require('./req-category.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /reqCategorys Get a list of reqCategorys
 * @apiVersion 0.1.0
 * @apiName GetReqCategorys
 * @apiDescription Get all the reqCategory.
 * @apiGroup ReqCategorys
 *
 */
exports.index = function (req, res) {
  ReqCategory.find(function (err, reqCategorys) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(reqCategorys);
  });
};

/**
 * @api {get} /reqCategorys/:id Get a single reqCategory
 * @apiVersion 0.1.0
 * @apiName GetReqCategory
 * @apiDescription Get only one unique element of reqCategory based on its unique id.
 * @apiGroup ReqCategorys
 *
 * @apiParam {String} id ReqCategorys unique id.
 *
 */
exports.show = function (req, res) {
  ReqCategory.findById(req.params.id, function (err, reqCategory) {
    if (err) { return handleError(res, err); }
    if (!reqCategory) { return res.status(404).end(); }
    return res.status(200).json(reqCategory);
  });
};

/**
 * @api {post} /reqCategorys Create a new reqCategory
 * @apiVersion 0.1.0
 * @apiName CreateReqCategory
 * @apiDescription Creates a new reqCategory.
 * @apiGroup ReqCategorys
 *
 */
exports.create = function (req, res) {
  ReqCategory.create(req.body, function (err, reqCategory) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(reqCategory);
  });
};

/**
 * @api {put} /reqCategorys/:id Updates an existing reqCategory
 * @apiVersion 0.1.0
 * @apiName UpdateReqCategory
 * @apiDescription Update an element of reqCategory based on its unique id.
 * @apiGroup ReqCategorys
 *
 * @apiParam {String} id ReqCategorys unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  ReqCategory.findById(req.params.id, function (err, reqCategory) {
    if (err) { return handleError(res, err); }
    if (!reqCategory) { return res.status(404).end(); }
    var updated = _.merge(reqCategory, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(reqCategory);
    });
  });
};

/**
 * @api {delete} /reqCategorys/:id Deletes a reqCategory
 * @apiVersion 0.1.0
 * @apiName RemoveReqCategory
 * @apiDescription Delete an element of reqCategory based on its unique id.
 * @apiGroup ReqCategorys
 *
 * @apiParam {String} id ReqCategorys unique id.
 *
 */
exports.destroy = function (req, res) {
  ReqCategory.findById(req.params.id, function (err, reqCategory) {
    if (err) { return handleError(res, err); }
    if (!reqCategory) { return res.status(404).end(); }
    reqCategory.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
