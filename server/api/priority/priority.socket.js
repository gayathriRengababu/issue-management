'use strict';

var Priority = require('./priority.model');

exports.register = function (socket) {

  Priority.schema.post('save', function (doc) {
    socket.emit('Priority:save', doc);
  });

  Priority.schema.post('remove', function (doc) {
    socket.emit('Priority:remove', doc);
  });

};
