'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PrioritySchema = new Schema({
  priority_type: {type : String, required : true}
});

module.exports = mongoose.model('Priority', PrioritySchema);
