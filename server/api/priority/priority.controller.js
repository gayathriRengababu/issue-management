'use strict';

var _ = require('lodash');
var Priority = require('./priority.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /prioritys Get a list of prioritys
 * @apiVersion 0.1.0
 * @apiName GetPrioritys
 * @apiDescription Get all the priority.
 * @apiGroup Prioritys
 *
 */
exports.index = function (req, res) {
  Priority.find(function (err, prioritys) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(prioritys);
  });
};

/**
 * @api {get} /prioritys/:id Get a single priority
 * @apiVersion 0.1.0
 * @apiName GetPriority
 * @apiDescription Get only one unique element of priority based on its unique id.
 * @apiGroup Prioritys
 *
 * @apiParam {String} id Prioritys unique id.
 *
 */
exports.show = function (req, res) {
  Priority.findById(req.params.id, function (err, priority) {
    if (err) { return handleError(res, err); }
    if (!priority) { return res.status(404).end(); }
    return res.status(200).json(priority);
  });
};

/**
 * @api {post} /prioritys Create a new priority
 * @apiVersion 0.1.0
 * @apiName CreatePriority
 * @apiDescription Creates a new priority.
 * @apiGroup Prioritys
 *
 */
exports.create = function (req, res) {
  Priority.create(req.body, function (err, priority) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(priority);
  });
};

/**
 * @api {put} /prioritys/:id Updates an existing priority
 * @apiVersion 0.1.0
 * @apiName UpdatePriority
 * @apiDescription Update an element of priority based on its unique id.
 * @apiGroup Prioritys
 *
 * @apiParam {String} id Prioritys unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Priority.findById(req.params.id, function (err, priority) {
    if (err) { return handleError(res, err); }
    if (!priority) { return res.status(404).end(); }
    var updated = _.merge(priority, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(priority);
    });
  });
};

/**
 * @api {delete} /prioritys/:id Deletes a priority
 * @apiVersion 0.1.0
 * @apiName RemovePriority
 * @apiDescription Delete an element of priority based on its unique id.
 * @apiGroup Prioritys
 *
 * @apiParam {String} id Prioritys unique id.
 *
 */
exports.destroy = function (req, res) {
  Priority.findById(req.params.id, function (err, priority) {
    if (err) { return handleError(res, err); }
    if (!priority) { return res.status(404).end(); }
    priority.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
