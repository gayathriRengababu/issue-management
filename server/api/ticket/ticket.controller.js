'use strict';

var _ = require('lodash');
var Ticket = require('./ticket.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /tickets Get a list of tickets
 * @apiVersion 0.1.0
 * @apiName GetTickets
 * @apiDescription Get all the ticket.
 * @apiGroup Tickets
 *
 */
exports.index = function (req, res) {
  Ticket.find(function (err, tickets) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(tickets);
  });
};

/**
 * @api {get} /tickets/:id Get a single ticket
 * @apiVersion 0.1.0
 * @apiName GetTicket
 * @apiDescription Get only one unique element of ticket based on its unique id.
 * @apiGroup Tickets
 *
 * @apiParam {String} id Tickets unique id.
 *
 */
exports.show = function (req, res) {
  Ticket.findById(req.params.id, function (err, ticket) {
    if (err) { return handleError(res, err); }
    if (!ticket) { return res.status(404).end(); }
    return res.status(200).json(ticket);
  });
};

/**
 * @api {post} /tickets Create a new ticket
 * @apiVersion 0.1.0
 * @apiName CreateTicket
 * @apiDescription Creates a new ticket.
 * @apiGroup Tickets
 *
 */
exports.create = function (req, res) {
  Ticket.create(req.body, function (err, ticket) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(ticket);
  });
};

/**
 * @api {post} /ticket/search Search Ticket
 * @apiVersion 0.1.0
 * @apiName searchTicket
 * @apiDescription Search for any element in Ticket
 * @apiGroup Ticket
 *
 * @apiParam {String} SearchCriteria
 *
 */
exports.search = function (req, res) {
  
    Ticket.find(req.body.SearchCriteria,function(err,ticket){
         if (err) res.json({ status: 'error', message:err });
        if(ticket){
            return res.status(200).json(ticket);

        }
        
    });
};

/**
 * @api {put} /tickets/:id Updates an existing ticket
 * @apiVersion 0.1.0
 * @apiName UpdateTicket
 * @apiDescription Update an element of ticket based on its unique id.
 * @apiGroup Tickets
 *
 * @apiParam {String} id Tickets unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Ticket.findById(req.params.id, function (err, ticket) {
    if (err) { return handleError(res, err); }
    if (!ticket) { return res.status(404).end(); }
    var updated = _.merge(ticket, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(ticket);
    });
  });
};

/**
 * @api {delete} /tickets/:id Deletes a ticket
 * @apiVersion 0.1.0
 * @apiName RemoveTicket
 * @apiDescription Delete an element of ticket based on its unique id.
 * @apiGroup Tickets
 *
 * @apiParam {String} id Tickets unique id.
 *
 */
exports.destroy = function (req, res) {
  Ticket.findById(req.params.id, function (err, ticket) {
    if (err) { return handleError(res, err); }
    if (!ticket) { return res.status(404).end(); }
    ticket.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
