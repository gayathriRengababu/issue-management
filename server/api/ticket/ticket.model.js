'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TicketSchema = new Schema({
  client_id : {type: mongoose.type.objectId , ref: 'Client'},
    product_id : {type : mongoose.type.ObjectId, ref : 'Product'},
    reqCategory : {type : mongoose.type.ObjectId, ref : 'ReqCategory'},
    severity : {type : mongoose.type.ObjectId, ref : 'Priority'},
    assigned_wr_type : {type : String},
    assigned_to : {type : String},
    status : {type : String},
    created_by : {type : String},
    created_date : {type : Date},
    updated_by : {type : String},
    updated_date : {type : Date},
    severity : {type : mongoose.type.ObjectId, ref : 'Priority'}
});

module.exports = mongoose.model('Ticket', TicketSchema);
