'use strict';

var _ = require('lodash');
var Client = require('./client.model');

function handleError (res, err) {
  return res.status(500).send(err);
}

/**
 * @api {get} /clients Get a list of clients
 * @apiVersion 0.1.0
 * @apiName GetClients
 * @apiDescription Get all the client.
 * @apiGroup Clients
 *
 */
exports.index = function (req, res) {
  Client.find(function (err, clients) {
    if (err) { return handleError(res, err); }
    return res.status(200).json(clients);
  });
};

/**
 * @api {get} /clients/:id Get a single client
 * @apiVersion 0.1.0
 * @apiName GetClient
 * @apiDescription Get only one unique element of client based on its unique id.
 * @apiGroup Clients
 *
 * @apiParam {String} id Clients unique id.
 *
 */
exports.show = function (req, res) {
  Client.findById(req.params.id, function (err, client) {
    if (err) { return handleError(res, err); }
    if (!client) { return res.status(404).end(); }
    return res.status(200).json(client);
  });
};

/**
 * @api {post} /clients Create a new client
 * @apiVersion 0.1.0
 * @apiName CreateClient
 * @apiDescription Creates a new client.
 * @apiGroup Clients
 *
 */
exports.create = function (req, res) {
  Client.create(req.body, function (err, client) {
    if (err) { return handleError(res, err); }
    return res.status(201).json(client);
  });
};

/**
 * @api {post} /client/search Search Client
 * @apiVersion 0.1.0
 * @apiName searchClient
 * @apiDescription Search for any element in Client
 * @apiGroup Client
 *
 * @apiParam {String} SearchCriteria
 *
 */
exports.search = function (req, res) {
  
    Client.find(req.body.SearchCriteria,function(err,client){
         if (err) res.json({ status: 'error', message:err });
        if(client){
            return res.status(200).json(client);

        }
        
    });
};

/**
 * @api {put} /clients/:id Updates an existing client
 * @apiVersion 0.1.0
 * @apiName UpdateClient
 * @apiDescription Update an element of client based on its unique id.
 * @apiGroup Clients
 *
 * @apiParam {String} id Clients unique id.
 *
 */
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id; }
  Client.findById(req.params.id, function (err, client) {
    if (err) { return handleError(res, err); }
    if (!client) { return res.status(404).end(); }
    var updated = _.merge(client, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(client);
    });
  });
};

/**
 * @api {delete} /clients/:id Deletes a client
 * @apiVersion 0.1.0
 * @apiName RemoveClient
 * @apiDescription Delete an element of client based on its unique id.
 * @apiGroup Clients
 *
 * @apiParam {String} id Clients unique id.
 *
 */
exports.destroy = function (req, res) {
  Client.findById(req.params.id, function (err, client) {
    if (err) { return handleError(res, err); }
    if (!client) { return res.status(404).end(); }
    client.remove(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(204).end();
    });
  });
};
