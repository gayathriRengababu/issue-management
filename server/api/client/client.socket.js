'use strict';

var Client = require('./client.model');

exports.register = function (socket) {

  Client.schema.post('save', function (doc) {
    socket.emit('Client:save', doc);
  });

  Client.schema.post('remove', function (doc) {
    socket.emit('Client:remove', doc);
  });

};
