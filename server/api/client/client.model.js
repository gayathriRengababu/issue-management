'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    client_name: {type: String},
    product_id : {type : mongoose.type.ObjectId, ref : 'Product'}
});

module.exports = mongoose.model('Client', ClientSchema);
