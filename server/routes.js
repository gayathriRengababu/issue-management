'use strict';

var config = require('./config/environment');

module.exports = function (app) {

  // API
  app.use('/api/ticketHistory', require('./api/ticket-history'));
  app.use('/api/status', require('./api/status'));
  app.use('/api/ticketAttachment', require('./api/ticket-attachment'));
  app.use('/api/groups', require('./api/group'));
  app.use('/api/ticketDescription', require('./api/ticket-description'));
  app.use('/api/ticketComments', require('./api/ticket-comment'));
  app.use('/api/tickets', require('./api/ticket'));
  app.use('/api/reqCategory', require('./api/req-category'));
  app.use('/api/roles', require('./api/role'));
  app.use('/api/priority', require('./api/priority'));
  app.use('/api/clients', require('./api/client'));
  app.use('/api/products', require('./api/product'));
  app.use('/api/users', require('./api/user'));

  // Auth
  app.use('/auth', require('./auth'));

  app.route('/:url(api|app|bower_components|assets)/*')
    .get(function (req, res) {
      res.status(404).end();
    });

  app.route('/*')
    .get(function (req, res) {
      res.sendFile(
        app.get('appPath') + '/index.html',
        { root: config.root }
      );
    });

};
