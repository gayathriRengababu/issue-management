'use strict';

angular.module('issueManagement')
  .factory('Role', function ($resource) {
    return $resource('/api/roles/:id', { id: '@_id' }, {
      update: {
        method: 'PUT'
      }
    });
  });
