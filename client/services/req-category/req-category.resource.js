'use strict';

angular.module('issueManagement')
  .factory('ReqCategory', function ($resource) {
    return $resource('/api/req-categorys/:id', { id: '@_id' }, {
      update: {
        method: 'PUT'
      }
    });
  });
