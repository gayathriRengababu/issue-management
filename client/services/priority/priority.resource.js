'use strict';

angular.module('issueManagement')
  .factory('Priority', function ($resource) {
    return $resource('/api/prioritys/:id', { id: '@_id' }, {
      update: {
        method: 'PUT'
      }
    });
  });
