'use strict';

angular.module('issueManagement')
  .factory('Client', function ($resource) {
    return $resource('/api/clients/:id', { id: '@_id' }, {
      update: {
        method: 'PUT'
      }
    });
  });
